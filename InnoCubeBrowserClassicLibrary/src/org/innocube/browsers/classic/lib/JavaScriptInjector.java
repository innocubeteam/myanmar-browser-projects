/*
 * Mandalay Browser for Android
 * 
 * Copyright (C) 2013 - to Aung Myo Aye.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package org.innocube.browsers.classic.lib;

import android.webkit.WebView;

public class JavaScriptInjector {
	public static void Inject(WebView webview,String scripts)
	{
		webview.loadUrl("javascript:" + scripts);
	}
}
