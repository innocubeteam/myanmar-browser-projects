/*
 * Zirco Browser for Android
 * 
 * Copyright (C) 2010 - 2011 J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.yangon.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.content.res.AssetManager;
import android.widget.Toast;

public class SetupFontFile {

	public static void SetupFontFiles(AssetManager assets , Context context)
	{
		File cacheFolder = context.getCacheDir();
		String assetFontPath = "fonts";		
		
		try {
				String[] fontFileNames =assets.list(assetFontPath);
		
				//copy font files
				for(String file : fontFileNames)
				{ 
					File fileToCopy = new File(cacheFolder + "/" + file);
					
					if(!fileToCopy.exists())
					{
						InputStream fontFile = assets.open(assetFontPath + "/" + file);
						OutputStream copyFileTo = new FileOutputStream(fileToCopy);
						
						 byte[] buffer = new byte[1024];
					     int read = fontFile.read(buffer);
					     
					     while (read != -1) 
					     {
					       copyFileTo.write(buffer, 0, read);
					       read = fontFile.read(buffer);
					    }
					    
					    copyFileTo.close();
					    fontFile.close();
					}
				}
		} catch (IOException e) {
			
			 Toast.makeText(context, e.getMessage() , Toast.LENGTH_LONG).show();
			 
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
