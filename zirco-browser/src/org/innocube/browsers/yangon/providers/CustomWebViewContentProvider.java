/*
 * Yangon Browser for Android
 * 
 * Copyright (C) 2013 InnoCube Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */


package org.innocube.browsers.yangon.providers;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.innocube.browsers.classic.lib.HtmlContentCreator;

import org.innocube.browsers.classic.lib.HtmlContentCreator.SCRIPT_TYPE;
import org.innocube.browsers.yangon.utils.IOUtils;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;

import android.os.MemoryFile;
import android.os.ParcelFileDescriptor;
import android.widget.Toast;


public class CustomWebViewContentProvider extends ContentProvider  {
	
	
	File CreateCacheFile(SCRIPT_TYPE scriptType ,String prefix , String suffix) throws IOException
	{
		File output = File.createTempFile(prefix, suffix, getContext().getCacheDir());
		FileOutputStream fileOutStream = new FileOutputStream(output);
		
		switch (scriptType) {
			case FONT_STYLE_SHEET:
			{	
				fileOutStream.write(HtmlContentCreator.GetHtmlContentStream(SCRIPT_TYPE.FONT_STYLE_SHEET));
			}break;
		}
		
		fileOutStream.close();
		
		return output;
	}
	
	@Override
	public android.os.ParcelFileDescriptor openFile(Uri uri, String mode) throws java.io.FileNotFoundException {
		
		Uri fontFileUri=null;
		ParcelFileDescriptor fileDescriptor=null;
		File file = null;
	
		
		if(uri.toString().contains("ZawgyiOne.ttf"))
		{
			
			fontFileUri = Uri.parse(getContext().getCacheDir().getAbsolutePath()  + "/ZawgyiOne.ttf");
			file = new File(fontFileUri.getPath());
			
			fileDescriptor = ParcelFileDescriptor.open(file, ParcelFileDescriptor.MODE_READ_ONLY);
		}
		else if(uri.toString().contains("mandalay_browser_css.css"))
		{
			try {
				
				fileDescriptor = ParcelFileDescriptor.open(CreateCacheFile(SCRIPT_TYPE.FONT_STYLE_SHEET,"mandalay_browser_css","css"), ParcelFileDescriptor.MODE_READ_ONLY);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
				
		}
		
		
		
		return fileDescriptor;
	};
	
	@Override
	public int delete(Uri arg0, String arg1, String[] arg2) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
