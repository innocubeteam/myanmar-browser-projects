/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.tint.addons;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class Controller {
	
	private final static String _WHITE_LIST_FILE = "-whitelist";
	
	/**
	 * Holder for singleton implementation.
	 */
	private static final class ControllerHolder {
		private static final Controller INSTANCE = new Controller();
		/**
		 * Private Constructor.
		 */
		private ControllerHolder() { }
	}
	
	/**
	 * Get the unique instance of the Controller.
	 * @return The instance of the Controller
	 */
	public static Controller getInstance() {
		return ControllerHolder.INSTANCE;
	}
	
	/**
	 * Private Constructor.
	 */
	private Controller() {
		mWhiteList = null;		
	}
	
	private List<String> mWhiteList;
	
	public List<String> getWhiteList(Context context) {
		if (mWhiteList == null) {
			loadWhiteList(context);
		}
		
		return mWhiteList;
	}
	
	public void addToWhiteList(Context context, String value) {
		mWhiteList.add(value);
		saveWhiteList(context);
	}
	
	public void removeFromWhiteList(Context context, int index) {
		mWhiteList.remove(index);
		saveWhiteList(context);
	}
	
	public void clearWhiteList(Context context) {
		mWhiteList.clear();
		saveWhiteList(context);
	}
	
	public void loadWhiteListDefaultValues(Context context) {
		loadWhiteListDefault();
		saveWhiteList(context);
	}
	
	private void saveWhiteList(Context context) {
		try {
			FileOutputStream fos = context.openFileOutput(_WHITE_LIST_FILE, Activity.MODE_PRIVATE);			
			
			if (fos != null) {
				for (String s : mWhiteList) {
					fos.write((s + "\n").getBytes());
				}
				
				fos.close();
			}
			
		} catch (FileNotFoundException e) {
			Log.w("saveWhiteList", "Unable to save  white list: " + e.getMessage());
		} catch (IOException e) {
			Log.w("saveWhiteList", "Unable to save  white list: " + e.getMessage());
		}
	}
	
	private void loadWhiteList(Context context) {
		
		mWhiteList = new ArrayList<String>();
		
		try {
			FileInputStream fis = context.openFileInput(_WHITE_LIST_FILE);
			
			if (fis != null) {
				BufferedReader reader = new BufferedReader(new InputStreamReader(fis));

				String line;

				while ((line = reader.readLine()) != null) {
					if (line.length() > 0) {
						mWhiteList.add(line);
					}
				}

				fis.close();
			} else {
				loadWhiteListDefault();
			}

		} catch (FileNotFoundException e) {
			loadWhiteListDefault();
		} catch (IOException e) {
			loadWhiteListDefault();
		}
	}
	
	private void loadWhiteListDefault() {
		mWhiteList.clear();
		mWhiteList.add("bbc.com");
		mWhiteList.add("cnn.com");		
		mWhiteList.add("nytimes.com");
		mWhiteList.add("foxnews.com");		
	}

}
