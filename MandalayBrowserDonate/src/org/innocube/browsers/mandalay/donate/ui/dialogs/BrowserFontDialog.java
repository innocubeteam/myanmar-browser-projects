/*
 * Tint Browser for Android\
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.mandalay.donate.ui.dialogs;


import java.lang.reflect.Array;
import java.util.Arrays;

import org.innocube.browsers.mandalay.donate.R;
import org.innocube.browsers.mandalay.donate.utils.ApplicationUtils;
import org.innocube.browsers.mandalay.donate.utils.Constants;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.widget.Toast;



public class BrowserFontDialog  {
	
	
	public static void DisplayFontDialog(final Activity activity)
	{		 		
		final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(activity);	
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		final String[] standardFontList=  activity.getResources()
			.getStringArray(R.array.PreferenceBrowserFontValues);
		
		final String currentFont = prefs.getString(Constants.PREFERENCE_BROWSER_FONT, "");
		
		builder.setTitle(R.string.FontChangeDialogTitle);
		builder.setSingleChoiceItems(R.array.PreferenceBrowserFontValues, 
				Arrays.asList(standardFontList).indexOf(currentFont)
		    , new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	
		    	if(!standardFontList[item].equals(currentFont))
		    	{
			    	Editor prefEditor= prefs.edit();
			    	prefEditor.putString(Constants.PREFERENCE_BROWSER_FONT, 
			    			standardFontList[item]);		    	
			    	prefEditor.commit();
			    	
			    	askForRestart(activity);
		    	}
		    	
		    	dialog.dismiss();
		    }
		});
		
		 
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	private static void askForRestart(final Activity activity) {
		ApplicationUtils.showYesNoDialog(activity,
				android.R.drawable.ic_dialog_alert,
				R.string.RestartDialogTitle,
				R.string.RestartDialogMessage,
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				
				PendingIntent intent = PendingIntent.getActivity(activity.getBaseContext(), 0, new Intent(activity.getIntent()), activity.getIntent().getFlags());
				AlarmManager mgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
				mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, intent);
				System.exit(2);
			}
		});
	}
	
}
