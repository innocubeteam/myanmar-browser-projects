/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.lib;

import org.innocube.browsers.lib.Enums.BrowserFontType;

import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

public class UIHelpers {
	
	//zawgyi true type font
	static String FontPathZawGyi ="02.dat";
	
	//unicode
	static String FontPathUniCode ="03.dat";
	
	public static void SetCustomTypeFace(TextView tv , AssetManager assetMgr , BrowserFontType fontType)
	{	
		switch(fontType)
		{
			case UNICODE:{tv.setTypeface(Typeface.createFromAsset(assetMgr, FontPathUniCode));}break;
			case ZAWGYI:{tv.setTypeface(Typeface.createFromAsset(assetMgr, FontPathZawGyi));}break;
		}
		
	}
	
	public static void SetCustomTypeFace(AutoCompleteTextView actv , AssetManager assetMgr , BrowserFontType fontType)
	{	
		switch(fontType)
		{
			case UNICODE:{actv.setTypeface(Typeface.createFromAsset(assetMgr, FontPathUniCode));}break;
			case ZAWGYI:{actv.setTypeface(Typeface.createFromAsset(assetMgr, FontPathZawGyi));}break;
		}
	}
	
	
	public static String AdjustZawGyiStringWithFixedWidth(String text, BrowserFontType fontType)
	{ 
		if(fontType== BrowserFontType.ZAWGYI)
		{
			text=text.replace("င္","င္​");
			text=text.replace("ေ","​ေ");
			text=text.replace("္","္​");
		}
		
		return text;
	}
}
