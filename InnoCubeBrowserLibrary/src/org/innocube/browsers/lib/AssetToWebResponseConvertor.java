/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.lib;

import java.io.IOException;

import android.content.res.AssetManager;
import android.util.Log;
import android.webkit.WebResourceResponse;
import android.widget.Toast;

import org.innocube.browsers.lib.Enums;
import org.innocube.browsers.lib.Enums.ContentType;

public class AssetToWebResponseConvertor {	
	public static WebResourceResponse Convert(AssetManager assetManager,
			String assetPath , String mime ,String encoding) 
	{       	   	   	
			try 
			{
				Log.d("Mandalay Browser", "AssetToWebResponseConvertor  - " +  assetPath);
				
				return new WebResourceResponse(encoding, mime , assetManager.open(assetPath));
			} catch (IOException e) {
				Log.d("Mandalay Browser", "AssetToWebResponseConvertor [Error] - " +  assetPath);
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
	
	}
}

