package org.innocube.browsers.lib;

public class Enums {
	public enum  ContentType { FONT_STYLE_SHEET_ZAWGYI , 
		FONT_STYLE_SHEET_UNICODE , LOAD_STANDARD_CSS_SCRIPT , LOAD_UNICODE_CSS_SCRIPT,  LOAD_WRAP_FONT_SCRIPT }
	
	public enum BrowserFontType { ZAWGYI , UNICODE};
}
