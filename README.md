Copyright and License

These projects contain code from the Tint Browser and zirco browser Project, which is subject to the copyright and license on the project site.

Project List 

* Mandalay Browser Donate (dir/ MandalayBrowserDonate)
* Mandalay Browser Free (dir/ TintBrowser)
* Yangon Browser (dir/ zirco-browser)
* Tint Brower Mobile View Addon (dir/ TintBrowserGoogleMobileViewAddon)

The rest of the code is Copyright 2013, Innocube Team, and available under the GPLV3. 
