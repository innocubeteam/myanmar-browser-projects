/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.classic.lib;

import java.io.InputStream;
import java.io.ByteArrayInputStream;

public class HtmlContentCreator {
	
	public enum  SCRIPT_TYPE { FIXED_ZAWGYI , FONT_STYLE_SHEET }
	
	
	public static byte[] GetHtmlContentStream(SCRIPT_TYPE st) 
	{
		switch(st)
		{
			case FONT_STYLE_SHEET:
			{
				return CreateFontStyleSheet().getBytes();		
			}
		}
		
		
		return null;
	}
	
	
	static String CreateFontStyleSheet()
	{
		return  "	@font-face {\r\n" + 
				"		font-family: \"Zawgyi-One\";\r\n" + 
				"		src: url(\"ZawgyiOne.ttf\")  format(\"truetype\");\r\n" + 
				"	}\r\n" + 
				"	html,body,input,* {font-family: \"Zawgyi-One\"!important;}\r\n"; 			
	}
}
