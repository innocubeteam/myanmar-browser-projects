/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.lib;

import java.io.InputStream;
import java.io.ByteArrayInputStream;

public class JavaScriptContentCreator {
	
	enum  SCRIPT_TYPE { FIXED_ZAWGYI , INJECT_CSS }
	
	
	public static InputStream GetJavaScriptStream(SCRIPT_TYPE st) 
	{
		InputStream inputStream = null;
		
		switch(st)
		{
			case FIXED_ZAWGYI:{
				inputStream = new ByteArrayInputStream(CreateZawGyiFixScript().getBytes());
			};break;
			case INJECT_CSS:{
				inputStream = new ByteArrayInputStream(CreateCSSScript().getBytes());
			};break;
		}
		
		return inputStream;
	}
	
	static String CreateZawGyiFixScript()
	{
		return  "var _0x9c9d=[\"\\x66\\x69\\x72\\x73\\x74\\x43\\x68\\x69\\x6C\\x64\",\"\\x6E\\x65\\x78\\x74\\x53\\x69\\x62\\x6C\\x69\\x6E\\x67\",\"\\x6E\\x6F\\x64\\x65\\x54\\x79\\x70\\x65\",\"\\x74\\x6F\\x4C\\x6F\\x77\\x65\\x72\\x43\\x61\\x73\\x65\",\"\\x74\\x61\\x67\\x4E\\x61\\x6D\\x65\",\"\\x73\\x63\\x72\\x69\\x70\\x74\",\"\\x73\\x74\\x79\\x6C\\x65\",\"\\x74\\x65\\x78\\x74\\x61\\x72\\x65\\x61\",\"\\x64\\x61\\x74\\x61\",\"\\x72\\x65\\x70\\x6C\\x61\\x63\\x65\",\"\\x62\\x6F\\x64\\x79\",\"\\u200B\\u1031\",\"\\u1039\\u200B\",\"\\u1004\\u1039\\u200B\",\"\\x72\\x65\\x61\\x64\\x79\\x53\\x74\\x61\\x74\\x65\",\"\\x63\\x6F\\x6D\\x70\\x6C\\x65\\x74\\x65\",\"\\x73\\x65\\x74\\x54\\x69\\x6D\\x65\\x6F\\x75\\x74\",\"\\x73\\x65\\x6E\\x64\",\"\\x70\\x72\\x6F\\x74\\x6F\\x74\\x79\\x70\\x65\",\"\\x6F\\x6E\\x72\\x65\\x61\\x64\\x79\\x73\\x74\\x61\\x74\\x65\\x63\\x68\\x61\\x6E\\x67\\x65\",\"\\x61\\x70\\x70\\x6C\\x79\"];function _replace_node_text(_0xc912x2,_0xc912x3,_0xc912x4){for(var _0xc912x5=_0xc912x2[_0x9c9d[0]];_0xc912x5!==null;_0xc912x5=_0xc912x5[_0x9c9d[1]]){if(_0xc912x5[_0x9c9d[2]]===1){var _0xc912x6=_0xc912x5[_0x9c9d[4]][_0x9c9d[3]]();if(_0xc912x6!==_0x9c9d[5]&&_0xc912x6!==_0x9c9d[6]&&_0xc912x6!==_0x9c9d[7]){_replace_node_text(_0xc912x5,_0xc912x3,_0xc912x4);} ;} else {if(_0xc912x5[_0x9c9d[2]]===3){_0xc912x5[_0x9c9d[8]]=_0xc912x5[_0x9c9d[8]][_0x9c9d[9]](_0xc912x3,_0xc912x4);} ;} ;} ;} ;function _fix_zawgyi(_0xc912x8){if(_0xc912x8){_replace_node_text(document[_0x9c9d[10]],/\\u1031/g,_0x9c9d[11]);_replace_node_text(document[_0x9c9d[10]],/\\u1039/g,_0x9c9d[12]);_replace_node_text(document[_0x9c9d[10]],/\\u1004\\u1039/g,_0x9c9d[13]);} else {if(document[_0x9c9d[14]]==_0x9c9d[15]){_fix_zawgyi(true);} else {if(document[_0x9c9d[10]]){_fix_zawgyi(true);} ;window[_0x9c9d[16]](_fix_zawgyi,1000);} ;} ;} ;function _interrupt_ajax(){_send=XMLHttpRequest[_0x9c9d[18]][_0x9c9d[17]];XMLHttpRequest[_0x9c9d[18]][_0x9c9d[17]]=function (){var _0xc912xa=this[_0x9c9d[19]];this[_0x9c9d[19]]=function (){if(this[_0x9c9d[14]]==4){_fix_zawgyi(true);} ;_0xc912xa[_0x9c9d[20]](this,arguments);} ;_send[_0x9c9d[20]](this,arguments);} ;} ;function _init(){_fix_zawgyi(document[_0x9c9d[14]]==_0x9c9d[15]);_interrupt_ajax();} ;";
	}
	
	static String CreateCSSScript()
	{
		return "var _0x2026=[\"\\x5A\\x61\\x77\\x67\\x79\\x69\\x2D\\x4F\\x6E\\x65\",\"\\x73\\x65\\x61\\x72\\x63\\x68\",\"\\x69\\x6E\\x6E\\x65\\x72\\x48\\x54\\x4D\\x4C\",\"\\x20\\x68\\x74\\x6D\\x6C\\x2C\\x62\\x6F\\x64\\x79\\x2C\\x69\\x66\\x72\\x61\\x6D\\x65\\x2C\\x2A\\x20\\x7B\\x66\\x6F\\x6E\\x74\\x2D\\x66\\x61\\x6D\\x69\\x6C\\x79\\x3A\\x20\\x22\\x5A\\x61\\x77\\x67\\x79\\x69\\x2D\\x4F\\x6E\\x65\\x22\\x21\\x69\\x6D\\x70\\x6F\\x72\\x74\\x61\\x6E\\x74\\x3B\\x7D\",\"\\x68\\x65\\x61\\x64\",\"\\x67\\x65\\x74\\x45\\x6C\\x65\\x6D\\x65\\x6E\\x74\\x73\\x42\\x79\\x54\\x61\\x67\\x4E\\x61\\x6D\\x65\",\"\\x27\\x3C\\x73\\x74\\x79\\x6C\\x65\\x20\\x74\\x79\\x70\\x65\\x3D\\x22\\x74\\x65\\x78\\x74\\x2F\\x63\\x73\\x73\\x22\\x20\\x63\\x68\\x61\\x72\\x73\\x65\\x74\\x3D\\x22\\x75\\x74\\x66\\x2D\\x38\\x22\\x3E\",\"\\x40\\x66\\x6F\\x6E\\x74\\x2D\\x66\\x61\\x63\\x65\\x20\\x7B\\x66\\x6F\\x6E\\x74\\x2D\\x66\\x61\\x6D\\x69\\x6C\\x79\\x3A\\x20\\x22\\x5A\\x61\\x77\\x67\\x79\\x69\\x2D\\x4F\\x6E\\x65\\x22\\x3B\\x20\",\"\\x73\\x72\\x63\\x3A\\x20\\x75\\x72\\x6C\\x28\\x22\\x5A\\x61\\x77\\x67\\x79\\x69\\x4F\\x6E\\x65\\x32\\x30\\x30\\x38\\x2E\\x74\\x74\\x66\\x22\\x29\\x20\\x20\\x66\\x6F\\x72\\x6D\\x61\\x74\\x28\\x22\\x74\\x72\\x75\\x65\\x74\\x79\\x70\\x65\\x22\\x29\\x20\\x2C\\x20\\x75\\x72\\x6C\\x28\\x22\\x7A\\x61\\x77\\x67\\x79\\x69\\x6F\\x6E\\x65\\x2D\\x77\\x65\\x62\\x66\\x6F\\x6E\\x74\\x2E\\x77\\x6F\\x66\\x66\\x22\\x29\\x20\\x20\\x66\\x6F\\x72\\x6D\\x61\\x74\\x28\\x22\\x77\\x6F\\x66\\x66\\x22\\x29\\x3B\",\"\\x7D\",\"\\x3C\\x2F\\x73\\x74\\x79\\x6C\\x65\\x3E\\x27\"];function _injectGlobalFontFaceIfRequired(headSection){if(headSection[_0x2026[2]][_0x2026[1]](_0x2026[0])){return _0x2026[3];} ;} ;function _injectFontCss(){var headSection=document[_0x2026[5]](_0x2026[4])[0];var script=_0x2026[6]+_0x2026[7]+_0x2026[8]+_0x2026[9]+_injectGlobalFontFaceIfRequired(headSection)+_0x2026[10];headSection[_0x2026[2]]=script+headSection[_0x2026[2]];} ;function _init(){_injectFontCss();} ;";
	}
}
