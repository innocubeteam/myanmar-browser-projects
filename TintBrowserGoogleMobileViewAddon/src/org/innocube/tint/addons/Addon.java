package org.innocube.tint.addons;

import java.util.ArrayList;
import java.util.List;

import org.tint.addons.framework.Action;
import org.tint.addons.framework.Callbacks;
import org.tint.addons.framework.LoadUrlAction;

import android.app.Service;
import android.content.Intent;
import android.os.RemoteException;


public class Addon extends BaseAddon {

	public Addon(Service service) {
		super(service);
	}

	@Override
	public int getCallbacks() throws RemoteException {
		// TODO Auto-generated method stub
		return Callbacks.PAGE_STARTED | Callbacks.HAS_SETTINGS_PAGE;
	}

	@Override
	public String getContributedBookmarkContextMenuItem(String currentTabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContributedHistoryBookmarksMenuItem(String currentTabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContributedHistoryContextMenuItem(String currentTabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContributedLinkContextMenuItem(String currentTabId, int hitTestResult, String url) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getContributedMainMenuItem(String currentTabId, String currentTitle, String currentUrl) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onBind() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Action> onContributedBookmarkContextMenuItemSelected(String currentTabId, String title, String url) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onContributedHistoryBookmarksMenuItemSelected(String currentTabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onContributedHistoryContextMenuItemSelected(String currentTabId, String title, String url) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onContributedLinkContextMenuItemSelected(String currentTabId, int hitTestResult, String url) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onContributedMainMenuItemSelected(String currentTabId, String currentTitle, String currentUrl) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onPageFinished(String tabId, String url) throws RemoteException {
		// TODO Auto-generated method stub
		
		return null;
	}
	
	boolean IsUrlNeedToParseAsMobileViewing(String url)
	{
		if(url.contains("www.google.com"))
			return false;
		else		
			return IsUrlInMobileViewList(url);		
	}
	
	String GetMobileViewUrl(String url)
	{
		return "http://www.google.com/gwt/x?u=" + url;
	}
	
	private boolean IsUrlInMobileViewList(String url) {
		List<String> whiteList = Controller.getInstance().getWhiteList(mService);

		for (String s : whiteList) {
			if (url.toLowerCase().contains(s.toLowerCase())) {
				return true;
			}
		}

		return false;
	}	
	
	@Override
	public List<Action> onPageStarted(String tabId, String url) throws RemoteException {
		// TODO Auto-generated method stub
		if(IsUrlNeedToParseAsMobileViewing(url))
		{		
			List<Action> response = new ArrayList<Action>();
			response.add(new LoadUrlAction(GetMobileViewUrl(url)));
			
			return response;
		}
		else
		{
			return null;
		}
	}
	
	@Override
	public List<Action> onTabClosed(String tabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Action> onTabOpened(String tabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Action> onTabSwitched(String tabId) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onUnbind() throws RemoteException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Action> onUserConfirm(String currentTabId, int questionId, boolean positiveAnswer) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Action> onUserInput(String currentTabId, int questionId, boolean cancelled, String userInput) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public List<Action> onUserChoice(String currentTabId, int questionId, boolean cancelled, int userChoice) throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void showAddonSettingsActivity() throws RemoteException {
		// TODO Auto-generated method stub
		Intent i = new Intent(mService, Preferences.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
		mService.startActivity(i);
	}

}
