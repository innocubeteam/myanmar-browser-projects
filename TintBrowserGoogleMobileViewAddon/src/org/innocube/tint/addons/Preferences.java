/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.tint.addons;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.innocube.tint.addons.R;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Browser;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

public class Preferences extends ListActivity {

	private static final int MENU_ADD = Menu.FIRST;
	private static final int MENU_CLEAR = Menu.FIRST + 1;
	private static final int MENU_RESET_TO_DEFAULT = Menu.FIRST + 2;
	
	private WhiteListAdaper mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setTitle(R.string.PreferencesActivityTitle);
		getActionBar().setSubtitle(R.string.PreferencesActivitySubTitle);
		
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mAdapter = new WhiteListAdaper(this, R.layout.adblocker_white_list_row, Controller.getInstance().getWhiteList(this));
		getListView().setAdapter(mAdapter);
        getListView().setOnItemClickListener(mAdapter);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		
		menu.add(0, MENU_ADD, 0, R.string.WhiteListMenuAdd);
		menu.add(0, MENU_CLEAR, 0, R.string.WhiteListMenuClear);
		menu.add(0, MENU_RESET_TO_DEFAULT, 0, R.string.WhiteListMenuReset);
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
    	case MENU_ADD:
    		addToWhiteList();
    		return true;
    		
    	case MENU_CLEAR:    		
    		clearWhiteList();
            return true;
            
    	case MENU_RESET_TO_DEFAULT:
    		resetToDefaultValues();
    		return true;
        default: return true;
    	}
	}
	
	private void doAddToWhiteList(String value) {
		Controller.getInstance().addToWhiteList(this, value);
		mAdapter.notifyDataSetChanged();
	}
	
	void BindAutoCompleteHistory(AutoCompleteTextView input)
	{		
		Cursor cursor = this.managedQuery(Browser.BOOKMARKS_URI,
                Browser.HISTORY_PROJECTION, null, null, null);
		ArrayList<String> sites = new ArrayList<String>();
		
        if (cursor.moveToFirst()) {
            while (cursor.isAfterLast() == false) {
            	URI uri;
            	
				try {
					uri = new URI(cursor
					        .getString(Browser.HISTORY_PROJECTION_URL_INDEX));
					
					String hostName =uri.getHost().replace("www.", ""); 
							
					if(sites.indexOf(hostName)==-1)
						sites.add(hostName);
					
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block				
				}
            	
            	            	
            	cursor.moveToNext();
            }
        }
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, sites);
        input.setAdapter(adapter);
	}
	
	/**
	 * Build and show a dialog for user input. Add user input to the white list.
	 */
	private void addToWhiteList() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);    
    	builder.setCancelable(true);
    	builder.setIcon(android.R.drawable.ic_input_add);
    	builder.setTitle(getResources().getString(R.string.WhiteListDialogAddTitle));
    	
    	builder.setInverseBackgroundForced(true);
    	
    	// Set an EditText view to get user input 
    	final AutoCompleteTextView input = new AutoCompleteTextView(this);
    	input.setInputType(InputType.TYPE_TEXT_VARIATION_URI);
    	BindAutoCompleteHistory(input);
    	builder.setView(input);
    	
    	builder.setInverseBackgroundForced(true);
    	builder.setPositiveButton(getResources().getString(R.string.OK), new DialogInterface.OnClickListener() {
    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    			doAddToWhiteList(input.getText().toString());
    		}
    	});
    	builder.setNegativeButton(getResources().getString(R.string.Cancel), new DialogInterface.OnClickListener() {
    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	});
    	AlertDialog alert = builder.create();
    	alert.show();

	}
	
	/**
	 * Clear the white list.
	 */
	private void doClearWhiteList() {		
		Controller.getInstance().clearWhiteList(this);
		mAdapter.notifyDataSetChanged();
	}
	
	private void doResetToDefaultValues() {
		Controller.getInstance().loadWhiteListDefaultValues(this);
		mAdapter.notifyDataSetChanged();
	}
	
	/**
	 * Display a confirmation dialog and clear the white list.
	 */
	private void clearWhiteList() {
		showYesNoDialog(
				R.string.WhiteListDialogClearTitle,
				R.string.CannotBeUndone,
				android.R.drawable.ic_dialog_alert,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						doClearWhiteList();
					}
		});		
    }
	
	private void resetToDefaultValues() {
		showYesNoDialog(
				R.string.WhiteListDialogResetTitle,
				R.string.CannotBeUndone,
				android.R.drawable.ic_dialog_alert,
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						doResetToDefaultValues();
					}
		});
	}
	
	private void showYesNoDialog(int title, int message, int icon, DialogInterface.OnClickListener onYes) {
		showYesNoDialog(title, getString(message), icon, onYes);
	}
	
	private void showYesNoDialog(int title, String message, int icon, DialogInterface.OnClickListener onYes) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setCancelable(true);
    	builder.setIcon(icon);
    	builder.setTitle(getResources().getString(title));
    	builder.setMessage(message);

    	builder.setInverseBackgroundForced(true);
    	builder.setPositiveButton(getResources().getString(R.string.Yes), onYes);
    	builder.setNegativeButton(getResources().getString(R.string.No), new DialogInterface.OnClickListener() {
    		@Override
    		public void onClick(DialogInterface dialog, int which) {
    			dialog.dismiss();
    		}
    	});
    	AlertDialog alert = builder.create();
    	alert.show();
	}

	class WhiteListAdaper extends ArrayAdapter<String> implements OnItemClickListener {
		
		private int mResource;
        private LayoutInflater mInflater;
        private List<String> mData;

		public WhiteListAdaper(Context context, int resource, List<String> data) {
			super(context, resource);
			
			mResource = resource;
			mData = data;
			mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		@Override
        public View getView(int position, View convertView, ViewGroup parent) {
			View view;
			
			if (convertView == null) {
                view = mInflater.inflate(mResource, parent, false);
            } else {
                view = convertView;
            }
			
			TextView v = (TextView) view.findViewById(R.id.AdblockerListUrlValue);
			v.setText(mData.get(position));
			
			return view;
		}

		@Override
		public int getCount() {
			if (mData != null) {
				return mData.size();
			} else {
				return 0;
			}
		}

		@Override
		public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
			showYesNoDialog(
					R.string.WhiteListDialogRemoveItemTitle,
					mData.get(position),
					android.R.drawable.ic_dialog_alert,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							Controller.getInstance().removeFromWhiteList(Preferences.this, position);
							notifyDataSetChanged();
						}
			});
		}
		
	}

}
