android.annotation.SuppressLint
android.annotation.TargetApi
org.innocube.tint.addons.BuildConfig
org.innocube.tint.addons.Controller:
    private static final java.lang.String _WHITE_LIST_FILE
org.innocube.tint.addons.Controller$ControllerHolder:
    43:43:private Controller$ControllerHolder()
org.innocube.tint.addons.Preferences:
    private static final int MENU_ADD
    private static final int MENU_CLEAR
    private static final int MENU_RESET_TO_DEFAULT
org.innocube.tint.addons.R
org.innocube.tint.addons.R$attr
org.innocube.tint.addons.R$drawable
org.innocube.tint.addons.R$id
org.innocube.tint.addons.R$layout
org.innocube.tint.addons.R$string
org.tint.addons.framework.Action:
    public static final int ACTION_NONE
    public static final int ACTION_SHOW_TOAST
    public static final int ACTION_SHOW_DIALOG
    public static final int ACTION_ASK_USER_CONFIRMATION
    public static final int ACTION_ASK_USER_INPUT
    public static final int ACTION_ASK_USER_CHOICE
    public static final int ACTION_OPEN_TAB
    public static final int ACTION_CLOSE_TAB
    public static final int ACTION_LOAD_URL
    public static final int ACTION_BROWSE_STOP
    public static final int ACTION_BROWSE_RELOAD
    public static final int ACTION_BROWSE_FORWARD
    public static final int ACTION_BROWSE_BACK
    97:97:public int getAction()
org.tint.addons.framework.AskUserChoiceAction:
    32:36:public AskUserChoiceAction(int,java.lang.String,java.util.List)
    47:47:public java.lang.String getTitle()
    51:51:public java.util.List getChoices()
org.tint.addons.framework.AskUserConfirmationAction:
    31:37:public AskUserConfirmationAction(int,java.lang.String,java.lang.String,java.lang.String,java.lang.String)
    49:49:public java.lang.String getTitle()
    53:53:public java.lang.String getMessage()
    57:57:public java.lang.String getPositiveButtonCaption()
    61:61:public java.lang.String getNegativeButtonCaption()
org.tint.addons.framework.AskUserInputAction:
    33:34:public AskUserInputAction(int,java.lang.String,java.lang.String)
    37:38:public AskUserInputAction(int,java.lang.String,java.lang.String,java.lang.String)
    41:42:public AskUserInputAction(int,java.lang.String,java.lang.String,java.lang.String,java.lang.String)
    45:52:public AskUserInputAction(int,java.lang.String,java.lang.String,java.lang.String,java.lang.String,int)
    65:65:public java.lang.String getTitle()
    69:69:public java.lang.String getMessage()
    73:73:public java.lang.String getInputHint()
    77:77:public java.lang.String getDefaultInput()
    81:81:public int getInputType()
org.tint.addons.framework.BaseAskUserAction:
    28:30:protected BaseAskUserAction(int,int)
    38:38:public int getId()
org.tint.addons.framework.Callbacks
org.tint.addons.framework.IAddon$Stub:
    private static final java.lang.String DESCRIPTOR
    static final int TRANSACTION_onBind
    static final int TRANSACTION_onUnbind
    static final int TRANSACTION_getName
    static final int TRANSACTION_getShortDescription
    static final int TRANSACTION_getDescription
    static final int TRANSACTION_getContact
    static final int TRANSACTION_getCallbacks
    static final int TRANSACTION_onPageStarted
    static final int TRANSACTION_onPageFinished
    static final int TRANSACTION_onTabOpened
    static final int TRANSACTION_onTabClosed
    static final int TRANSACTION_onTabSwitched
    static final int TRANSACTION_getContributedMainMenuItem
    static final int TRANSACTION_onContributedMainMenuItemSelected
    static final int TRANSACTION_getContributedLinkContextMenuItem
    static final int TRANSACTION_onContributedLinkContextMenuItemSelected
    static final int TRANSACTION_getContributedHistoryBookmarksMenuItem
    static final int TRANSACTION_onContributedHistoryBookmarksMenuItemSelected
    static final int TRANSACTION_getContributedBookmarkContextMenuItem
    static final int TRANSACTION_onContributedBookmarkContextMenuItemSelected
    static final int TRANSACTION_getContributedHistoryContextMenuItem
    static final int TRANSACTION_onContributedHistoryContextMenuItemSelected
    static final int TRANSACTION_onUserConfirm
    static final int TRANSACTION_onUserInput
    static final int TRANSACTION_onUserChoice
    static final int TRANSACTION_showAddonSettingsActivity
    26:33:public static org.tint.addons.framework.IAddon asInterface(android.os.IBinder)
org.tint.addons.framework.IAddon$Stub$Proxy
org.tint.addons.framework.LoadUrlAction:
    33:34:public LoadUrlAction(java.lang.String,java.lang.String)
    37:38:public LoadUrlAction(java.lang.String,boolean)
    55:55:public java.lang.String getUrl()
    59:59:public boolean getLoadRawUrl()
org.tint.addons.framework.OpenTabAction:
    44:44:public java.lang.String getUrl()
org.tint.addons.framework.R
org.tint.addons.framework.R$drawable
org.tint.addons.framework.ShowDialogAction:
    29:33:public ShowDialogAction(java.lang.String,java.lang.String)
    43:43:public java.lang.String getTitle()
    47:47:public java.lang.String getMessage()
org.tint.addons.framework.ShowToastAction:
    30:31:public ShowToastAction(java.lang.String)
    34:44:public ShowToastAction(java.lang.String,int)
    59:59:public java.lang.String getMessage()
    63:63:public int getLength()
org.tint.addons.framework.TabAction:
    10:11:public TabAction(int)
    24:24:public java.lang.String getTabId()
    34:34:public static org.tint.addons.framework.TabAction createCloseTabAction()
    38:38:public static org.tint.addons.framework.TabAction createCloseTabAction(java.lang.String)
    42:42:public static org.tint.addons.framework.TabAction createBrowseStopAction()
    46:46:public static org.tint.addons.framework.TabAction createBrowseStopAction(java.lang.String)
    50:50:public static org.tint.addons.framework.TabAction createBrowseReloadAction()
    54:54:public static org.tint.addons.framework.TabAction createBrowseReloadAction(java.lang.String)
    58:58:public static org.tint.addons.framework.TabAction createBrowseForwardAction()
    62:62:public static org.tint.addons.framework.TabAction createBrowseForwardAction(java.lang.String)
    66:66:public static org.tint.addons.framework.TabAction createBrowseBackAction()
    70:70:public static org.tint.addons.framework.TabAction createBrowseBackAction(java.lang.String)
