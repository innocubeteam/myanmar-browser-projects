/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.mandalay.donate.ui.fragments;

import java.util.List;

import org.innocube.browsers.mandalay.donate.addons.AddonMenuItem;
import org.innocube.browsers.mandalay.donate.controllers.Controller;
import org.innocube.browsers.mandalay.donate.model.BookmarkHistoryItem;
import org.innocube.browsers.mandalay.donate.model.BookmarksAdapter;
import org.innocube.browsers.mandalay.donate.providers.BookmarksProvider;
import org.innocube.browsers.mandalay.donate.providers.BookmarksWrapper;
import org.innocube.browsers.mandalay.donate.ui.activities.EditBookmarkActivity;
import org.innocube.browsers.mandalay.donate.ui.activities.TintBrowserActivity;
import org.innocube.browsers.mandalay.donate.ui.managers.UIManager;
import org.innocube.browsers.mandalay.donate.utils.ApplicationUtils;
import org.innocube.browsers.mandalay.donate.utils.Constants;
import org.innocube.browsers.mandalay.donate.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.database.Cursor;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;

public abstract class StartPageFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int CONTEXT_MENU_OPEN_IN_TAB = Menu.FIRST;	
	private static final int CONTEXT_MENU_COPY_URL = Menu.FIRST + 2;
	private static final int CONTEXT_MENU_SHARE_URL = Menu.FIRST + 3;	
	
	public interface OnStartPageItemClickedListener {
		public void onStartPageItemClicked(String url);
	}
	
	private View mParentView = null;
	
	private GridView mGrid;	
	private BookmarksAdapter mAdapter;
	
	private OnStartPageItemClickedListener 		mItemClickListener = null;	
	
	private OnSharedPreferenceChangeListener mPreferenceChangeListener;
	
	protected UIManager mUIManager;
	
	private boolean mInitialized;
	
	private boolean mListShown = true;
	
	public StartPageFragment() {
		mInitialized = false;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		
		if (!mInitialized) {
			try {
				mUIManager = ((TintBrowserActivity) activity).getUIManager();
			} catch (ClassCastException e) {
				Log.e("StartPageFragment.onAttach()", e.getMessage());
			}
			
			mInitialized = true;
		}		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if (mParentView == null) {		
			mParentView = inflater.inflate(getStartPageFragmentLayout(), container, false);
			mGrid = (GridView) mParentView.findViewById(R.id.StartPageFragmentGrid);
			
			String[] from = new String[] { BookmarksProvider.Columns.TITLE, BookmarksProvider.Columns.URL };
			int[] to = new int[] { R.id.StartPageRowTitle, R.id.StartPageRowUrl };
			
			mAdapter = new BookmarksAdapter(
					getActivity(),
					R.layout.start_page_row,
					null,
					from,
					to,
					0,
					R.drawable.browser_thumbnail);
			
			mGrid.setAdapter(mAdapter);
			
			mGrid.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
					
					if (mItemClickListener != null) {
						BookmarkHistoryItem item = BookmarksWrapper.getBookmarkById(getActivity().getContentResolver(), id);

						if (item != null) {
							mItemClickListener.onStartPageItemClicked(item.getUrl());
						}
					}					
				}
			});			
											
			mGrid.setOnTouchListener(mUIManager);		
			
			registerForContextMenu(mGrid);
			
			mPreferenceChangeListener = new OnSharedPreferenceChangeListener() {
				@Override
				public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
					if (Constants.PREFERENCE_START_PAGE_LIMIT.equals(key)) {
						getLoaderManager().restartLoader(0, null, StartPageFragment.this);
					}
				}			
			};

			PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(mPreferenceChangeListener);
		}
		
		return mParentView;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		setListShown(false);
		
		mParentView.postDelayed(new Runnable() {
			
			@Override
			public void run() {
				if (isAdded()) {
					getLoaderManager().initLoader(0, null, StartPageFragment.this);
				}
			}
		}, 100);
	}	

	@Override
	public void onDestroy() {
		PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(mPreferenceChangeListener);
		super.onDestroy();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		int limit;
		try {
			limit = Integer.parseInt(
					PreferenceManager.getDefaultSharedPreferences(getActivity()).getString(
							Constants.PREFERENCE_START_PAGE_LIMIT,
							Integer.toString(getResources().getInteger(R.integer.default_start_page_items_number))));
		} catch (Exception e) {
			limit = getResources().getInteger(R.integer.default_start_page_items_number);
		}
		
		return BookmarksWrapper.getCursorLoaderForStartPage(getActivity(), limit);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
		setListShown(true);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}
	
	public void setOnStartPageItemClickedListener(OnStartPageItemClickedListener listener) {
		mItemClickListener = listener;
	}
		
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		long id = ((AdapterContextMenuInfo) menuInfo).id;
		
		if (id != -1) {			
			BookmarkHistoryItem selectedItem = BookmarksWrapper.getBookmarkById(getActivity().getContentResolver(), id);
			
			if (selectedItem != null) {

				menu.setHeaderTitle(selectedItem.getTitle());

				
				BitmapDrawable icon = ApplicationUtils.getApplicationButtonImage(getActivity(), selectedItem.getFavicon());
				
				if (icon != null) {
						menu.setHeaderIcon(icon);
				}					
				
				menu.add(0, CONTEXT_MENU_OPEN_IN_TAB, 0, R.string.OpenInTab);				
				menu.add(0, CONTEXT_MENU_COPY_URL, 0, R.string.CopyUrl);
				menu.add(0, CONTEXT_MENU_SHARE_URL, 0, R.string.ContextMenuShareUrl);
				

				List<AddonMenuItem> addonsContributions = Controller.getInstance().getAddonManager().getContributedBookmarkContextMenuItems(mUIManager.getCurrentWebView());
				
				for (AddonMenuItem item : addonsContributions) {
					menu.add(0, item.getAddon().getMenuId(), 0, item.getMenuItem());
				}
			}
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		final AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		BookmarkHistoryItem selectedItem = BookmarksWrapper.getBookmarkById(getActivity().getContentResolver(), info.id);
		Intent i;
		
		switch (item.getItemId()) {
		case CONTEXT_MENU_OPEN_IN_TAB:			
			if (item != null) {
				/*
				Intent result = new Intent();
				result.putExtra(Constants.EXTRA_NEW_TAB, true);
				result.putExtra(Constants.EXTRA_URL, selectedItem.getUrl());

				getActivity().setResult(Activity.RESULT_OK, result);
				getActivity().finish();
				*/				
				
				mUIManager.addTab(selectedItem.getUrl(), false, PreferenceManager.getDefaultSharedPreferences(mUIManager.getMainActivity()).
						getBoolean(Constants.PREFERENCE_INCOGNITO_BY_DEFAULT, false));
			}
			return true;

		case CONTEXT_MENU_COPY_URL:
			if (selectedItem != null) {
				ApplicationUtils.copyTextToClipboard(getActivity(), selectedItem.getUrl(), getActivity().getResources().getString(R.string.UrlCopyToastMessage));
			}

			return true;

		case CONTEXT_MENU_SHARE_URL:
			if (selectedItem != null) {
				ApplicationUtils.sharePage(getActivity(), null, selectedItem.getUrl());						
			}

			return true;	

		default:
			if (Controller.getInstance().getAddonManager().onContributedBookmarkContextMenuItemSelected(
					getActivity(),
					item.getItemId(),
					selectedItem.getTitle(),
					selectedItem.getUrl(),
					mUIManager.getCurrentWebView())) {
				return true;
			} else {
				return super.onContextItemSelected(item);
			}
		}
	}

	
	protected abstract int getStartPageFragmentLayout();
	
	private void setListShown(boolean shown) {
		
		if (mListShown == shown) {
			return;
		}
		
		mListShown = shown;
		
		if (shown) {
			mGrid.setVisibility(View.VISIBLE);
		} else {
			mGrid.setVisibility(View.GONE);
		}
	}

}
