/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.lib;

import java.io.InputStream;
import java.io.ByteArrayInputStream;
import org.innocube.browsers.lib.Enums.ContentType;

public class HtmlContentCreator {	
	
	public static String GetHtmlContentString(ContentType contentType) 
	{
		String returnString="";
		
		switch(contentType)
		{
			case LOAD_STANDARD_CSS_SCRIPT:
			{				
				returnString =LoadStandardCompatibleScript(); //compatiable version	//LoadStandardFontScript(); old version
			}break;
			case LOAD_UNICODE_CSS_SCRIPT:
			{				
				returnString =	LoadUniCodeFontScript();
			}break;
			case LOAD_WRAP_FONT_SCRIPT:
			{
				returnString =	WrapFontScript();
			}break;
		default:
			break;
		}
		
		return returnString;
	}
	
	
	static String WrapFontScript()
	{
		return "";
	}
	
	static String LoadStandardCompatibleScript()  
	{
		return "try{var mandalay_browser_css = document.createElement('style');\r\n" + 			
				"mandalay_browser_css.innerHTML = '@font-face {font-family: \\'ZawGyi\\'; src: url(\\'zawgyi.svg\\') format(\\'svg\\'),url(\\'zawgyi.woff\\') format(\\'woff\\');} html,body,* { font-family:\\'ZawGyi\\' !important; }';" +
				"document.getElementsByTagName('head')[0].appendChild(mandalay_browser_css);} catch(err) {  }";
		
	}
	
	static String LoadStandardFontScript()
	{
		return "var mandalay_browser_css = document.createElement('link');\r\n" + 
				"mandalay_browser_css.id='mandalay_browser_css';" +
				"mandalay_browser_css.type = 'text/css';\r\n" + 
				"mandalay_browser_css.rel='stylesheet';\r\n" + 
				"mandalay_browser_css.media='all';\r\n" + 
				"mandalay_browser_css.href='zawgyi.css';\r\n" + 
				"document.getElementsByTagName('head')[0].appendChild(mandalay_browser_css);";
	}
	 
	static String LoadUniCodeFontScript()
	{
		return "var mandalay_browser_css = document.createElement('link');\r\n" + 
				"mandalay_browser_css.id='mandalay_browser_css';" +
				"mandalay_browser_css.type = 'text/css';\r\n" + 
				"mandalay_browser_css.rel='stylesheet';\r\n" + 
				"mandalay_browser_css.media='all';\r\n" + 
				"mandalay_browser_css.href='padauk.css';\r\n" + 
				"document.getElementsByTagName('head')[0].appendChild(mandalay_browser_css);";
	}	
}
