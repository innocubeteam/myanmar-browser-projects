/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.lib;

import org.innocube.browsers.lib.Enums.BrowserFontType;
import org.innocube.browsers.lib.Enums.ContentType;

import android.util.Log;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.Toast;


public class CustomWebViewClientEventHandler {
	public static void InitializeMyanmarFontAssets(WebView webview, String url , BrowserFontType fontType)
	{
		switch(fontType)
		{
			case UNICODE:{
				JavaScriptInjector.Inject(webview,
						HtmlContentCreator.GetHtmlContentString(ContentType.LOAD_UNICODE_CSS_SCRIPT)
					);
			}break;
			case ZAWGYI:{
				JavaScriptInjector.Inject(webview,
						HtmlContentCreator.GetHtmlContentString(ContentType.LOAD_STANDARD_CSS_SCRIPT)
					);		
			}break;
		}
	} 

	public static WebResourceResponse HandleShouldInterceptRequest(WebView webview, String url) {
		
		Log.d("Mandalay Browser", "HandleShouldInterceptRequest - " + url);
		
		url = url.toLowerCase();
		 
		if(url.contains("zawgyi.ttf"))  
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"02.dat","application/x-font-truetype","utf-8");
		else if(url.contains("zawgyi.svg"))			
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"01.dat","image/svg+xml","utf-8");
		else if(url.contains("unicode.ttf"))
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"03.dat","x-font-truetype","utf-8");
		else if(url.contains("unicode.svg"))  
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"04.dat","image/svg+xml","utf-8");  
		else if(url.contains("zawgyi.woff"))  
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"05.dat","application/x-font-woff","utf-8");
		else if(url.contains("zawgyi.eot"))  
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"06.dat"," application/vnd.ms-fontobject.eot","utf-8");
		else if(url.contains("zawgyi.css")) 
			//Log.d("Mandalay Browser", "HandleShouldInterceptRequest [ZawGyi] - " + url);
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"zawgyi.css","text/css","utf-8");
		else if(url.contains("padauk.css")) 
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"padauk.css","text/css","utf-8");	
		/*
		else if(url.contains("angular.min.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/angular.min.js","application/javascript","utf-8");
		else if(url.contains("cloudflare.min.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/cloudflare.min.js","application/javascript","utf-8");
		else if(url.contains("dojo.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/dojo.js","application/javascript","utf-8");
		else if(url.contains("ext-core.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/ext-core.js","application/javascript","utf-8");
		else if(url.contains("jquery.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/jquery.js","application/javascript","utf-8");
		else if(url.contains("jquery.min.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/jquery.min.js","application/javascript","utf-8");
		else if(url.contains("jquery-ui.min.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/jquery-ui.min.js","application/javascript","utf-8");
		else if(url.contains("mootools-core.js"))				
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/mootools-core.js","application/javascript","utf-8");
		else if(url.contains("mootools-more.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/mootools-more.js","application/javascript","utf-8");
		else if(url.contains("mootools.x.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/mootools.x.js","application/javascript","utf-8");
		else if(url.contains("mootools-yui-compressed.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/mootools-yui-compressed.js","application/javascript","utf-8");
		else if(url.contains("scriptaculous.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/scriptaculous.js","application/javascript","utf-8");
		else if(url.contains("swfobject.js"))		
			return AssetToWebResponseConvertor.Convert(webview.getContext().getAssets(),"sp/swfobject.js","application/javascript","utf-8");
		*/
		else 
			return null;
			
	}
}
