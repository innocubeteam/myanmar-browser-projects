/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.classic.lib;

public class JavaScriptContentCreator {
	
	public enum  SCRIPT_TYPE { FIXED_ZAWGYI , INJECT_CSS }
	
	public static String GetJavaScriptStream(SCRIPT_TYPE st) 
	{
		
		switch(st)
		{
			case FIXED_ZAWGYI:{
				return CreateZawGyiFixScript();
			}
			case INJECT_CSS:{
				return CreateCSSScript();
			}
		}
		
		return "";
	}
	
	static String CreateZawGyiFixScript()
	{
		return  "function _replace_node_text(parent, needle, replacement) {\r\n" + 
				"	\r\n" + 
				"    for (var child= parent.firstChild; child!==null; child= child.nextSibling) \r\n" + 
				"    {\r\n" + 
				"        if (child.nodeType===1) { //# Node.ELEMENT_NODE\r\n" + 
				"            var tag= child.tagName.toLowerCase();\r\n" + 
				"            if (tag!=='script' && tag!=='style' && tag!=='textarea')\r\n" + 
				"            	_replace_node_text(child, needle, replacement);\r\n" + 
				"        }\r\n" + 
				"        else if (child.nodeType===3) //text node\r\n" + 
				"            child.data= child.data.replace(needle, replacement);\r\n" + 
				"    }\r\n" + 
				"} \r\n" + 
				"\r\n" + 
				"\r\n" + 
				"function _fix_zawgyi(force)\r\n" + 
				"{	\r\n" + 
				"	if(force)\r\n" + 
				"	{		\r\n" + 
				"		_replace_node_text(document.body,/\\u1031/g,\"\\u200b\\u1031\");\r\n" + 
				"		_replace_node_text(document.body,/\\u1039/g,\"\\u1039\\u200b\");\r\n" + 
				"		_replace_node_text(document.body,/\\u1004\\u1039/g,\"\\u1004\\u1039\\u200b\");\r\n" + 
				"	}\r\n" + 
				"	else\r\n" + 
				"	{\r\n" + 
				"		if(document.readyState==\"complete\")\r\n" + 
				"			_fix_zawgyi(true);\r\n" + 
				"		else\r\n" + 
				"			window.setTimeout(_fix_zawgyi,1000);\r\n" + 
				"	}\r\n" + 
				"}\r\n" + 
				"\r\n" + 
				"function _interrupt_ajax()\r\n" + 
				"{\r\n" + 
				"	_send = XMLHttpRequest.prototype.send;\r\n" + 
				"	\r\n" + 
				"	XMLHttpRequest.prototype.send = function() {\r\n" + 
				"	\r\n" + 
				"	    var callback = this.onreadystatechange;\r\n" + 
				"	    this.onreadystatechange = function() {             \r\n" + 
				"	         if (this.readyState == 4) {	        	\r\n" + 
				"	        	 _fix_zawgyi(true);\r\n" + 
				"	         }\r\n" + 
				"\r\n" + 
				"	         callback.apply(this, arguments);\r\n" + 
				"	    }\r\n" + 
				"\r\n" + 
				"	    _send.apply(this, arguments);\r\n" + 
				"	}	\r\n" + 
				"}\r\n" + 
				"\r\n" + 
				"_fix_zawgyi(document.readyState==\"complete\");\r\n" + 
				"_interrupt_ajax();\r\n";
	}
	
	static String CreateCSSScript()
	{
		return "function _injectGlobalFontFaceIfRequired(headSection)\r\n" + 
				"{\r\n" + 
				"	if(headSection.innerHTML.search(\"Zawgyi-One\"))\r\n" + 
				"	{\r\n" + 
				"		return \" html,body,iframe,* {font-family: \\\"Zawgyi-One\\\"!important;}\";\r\n" + 
				"	}\r\n" + 
				"}\r\n" + 
				"\r\n" + 
				"function _injectFontCss()\r\n" + 
				"{\r\n" + 
				"	var headSection= document.getElementsByTagName(\"head\")[0];\r\n" + 
				"	\r\n" + 
				"	var script=\r\n" + 
				"			\"'<style type=\\\"text/css\\\" charset=\\\"utf-8\\\">\" +\r\n" + 
				"				\"@font-face {font-family: \\\"Zawgyi-One\\\"; \" +						\r\n" + 
				"				\"src: url(\\\"content://org.innocube.browsers.yangon.localfiles/ZawgyiOne2008.ttf\\\")  format(\\\"truetype\\\") , url(\\\"content://org.innocube.browsers.yangon.localfiles/zawgyione-webfont.woff\\\")  format(\\\"woff\\\");\" +\r\n" + 
				"				\"}\" + \r\n" + 
				"				_injectGlobalFontFaceIfRequired(headSection) + \r\n" + 
				"			  \"</style>'\";\r\n" + 
				"\r\n" + 
				"	\r\n" + 
				"	headSection.innerHTML=  script + headSection.innerHTML;	\r\n" + 
				"}\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"_injectFontCss();\r\n";
	}
}
