/*
 * Tint Browser for Android
 * 
 * Copyright (C) 2012 - to infinity and beyond J. Devauchelle and contributors.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

package org.innocube.browsers.classic.lib;



import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

public class UIHelpers {
	
	static String FontPath ="fonts/ZawgyiOne.ttf";
	
	public static void SetCustomTypeFace(TextView tv , AssetManager assetMgr)
	{	
		tv.setTypeface(Typeface.createFromAsset(assetMgr, FontPath));
	}
	
	public static void SetCustomTypeFace(AutoCompleteTextView actv , AssetManager assetMgr)
	{	
		actv.setTypeface(Typeface.createFromAsset(assetMgr, FontPath));
	}
	
	public static String AdjustZawGyiStringWithFixedWidth(String text)
	{ 
		text=text.replace("င္","င္​");
		text=text.replace("ေ","​ေ");
		text=text.replace("္","္​");
		
		return text;
	}
}
